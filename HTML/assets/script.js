/*
 * Game - remember the pattern
 *
 * @author Dominik Siwek <dominik.siwek1@wp.pl>
 * @copyright 2022 Dominik Siwek
 *
 */

let stepNumber = 5;
let animationSpeed = 500;
let animationActive = false;
let arrayAnimation = [];
let userArrayAnimation = [];
let blockButtons = false;
let endGame = false;
let startUserSelect = false;
let startUserPattern = false;

const changeSteps = (e, f) => {
  if (!blockButtons) {
    let step;
    if (f) step = 5;
    else step = e.options[e.selectedIndex].text;
    const correctBox = $(".correct");
    correctBox.map(function (i, e) {
      const items = $(e).find("span");
      if (step > items.length) {
        for (let i = items.length; i < step; i++) {
          $(e).append("<span></span>");
        }
      } else {
        for (let i = items.length; i >= step; i--) {
          $($(e).find("span")[i]).remove();
        }
      }
      stepNumber = step;
    });
  } else {
    alert(
      "Uwaga! Masz uruchomioną grę. Zresetuj ustawienia a następnie wybierz ilość kroków.",
    );
    $(`#steps option[value=${stepNumber}]`).prop("selected", true);
  }
};

const startAnimation = (check) => {
  if (endGame) {
    resetAnimation(false);
    endGame = false;
    startAnimation(false);
  } else {
    if (!animationActive && stepNumber >= arrayAnimation.length + 1 && !check) {
      animationActive = true;
      let random = getRandom(1, 17);
      if (random === arrayAnimation[arrayAnimation.length - 1]) {
        animationActive = false;
        startAnimation(false);
      } else {
        arrayAnimation.push(random);
        showSteps();
      }
    }
  }
  blockButtons = true;
};

const resetAnimation = (f) => {
  if (!animationActive) {
    arrayAnimation = [];
    userArrayAnimation = [];
    animationActive = false;
    startUserPattern = false;
    $(".correct").find("span").removeClass("green");
    blockButtons = false;
    if (f) {
      $("#steps").val(5);
      $("#level").val(500);
      stepNumber = 5;
      animationSpeed = 500;
      changeSteps(undefined, true);
    }
  }
};

const showSteps = () => {
  blockButtons = true;
  startUserPattern = false;
  arrayAnimation.map((e, i) => {
    setTimeout(
      () => {
        $(`.buttons-play span[data-value=${e}]`).addClass("active");
        var audio = document.getElementById(`sound${e}`);
        audio.play();
        setTimeout(() => {
          $(`.buttons-play span[data-value=${e}]`).removeClass("active");
        }, animationSpeed - 50);
      },
      i * animationSpeed + 50,
    );
  });
  setTimeout(
    () => {
      animationActive = false;
      startUserPattern = true;
    },
    arrayAnimation.length * animationSpeed + 200,
  );
};

const userSelect = (e) => {
  var audio = document.getElementById(`sound${e}`);
  audio.play();
  userArrayAnimation.push(Number(e));
  startUserSelect = true;
  if (
    userArrayAnimation.length > 0 &&
    arrayAnimation.length === userArrayAnimation.length
  ) {
    setTimeout(() => {
      if (checkArrayEquals()) {
        timeoutUser();
      } else {
        userArrayAnimation = [];
        showSteps();
      }
    }, 500);
  }
};

const timeoutUser = () => {
  if (Number(stepNumber) === userArrayAnimation.length) {
    setCorrectCircle();
    endGame = true;
    $(".start").removeClass("active");
    setTimeout(() => {
      if (
        confirm(
          "Gra zakończona. Gratulacje :) Wciśnij OK aby rozpocząć kolejną runde.",
        ) === true
      ) {
        setTimeout(() => {
          $(".start").click();
        }, 500);
      } else {
        resetAnimation(false);
      }
    }, 500);
  } else {
    setCorrectCircle();
    userArrayAnimation = [];
    startAnimation(false);
  }
};

const setCorrectCircle = () => {
  const correctBox = $(".correct");
  correctBox.map(function (i, e) {
    $($(e).find("span")[userArrayAnimation.length - 1]).addClass("green");
  });
};

const checkArrayEquals = () => {
  return (
    Array.isArray(arrayAnimation) &&
    Array.isArray(userArrayAnimation) &&
    arrayAnimation.length === userArrayAnimation.length &&
    arrayAnimation.every((val, index) => val === userArrayAnimation[index])
  );
};

const getRandom = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

$(document).ready(function () {
  $("#steps").on("change", function () {
    changeSteps(this, false);
  });
  $("#level").on("change", function () {
    if (!blockButtons || endGame) {
      animationSpeed = $(this).val();
    } else {
      alert(
        "Uwaga! Masz uruchomioną grę. Zresetuj ustawienia a następnie wybierz poziom trudności",
      );
      $(this).find(` option[value=${animationSpeed}]`).prop("selected", true);
    }
  });
  $(".start").on("click", function () {
    if ((!$(this).hasClass("active") && !animationActive) || endGame) {
      $(this).addClass("active");
      $(".reset").removeClass("active");
      startAnimation(false);
    }
  });
  $(".reset").on("click", function () {
    if (!animationActive) {
      $(".start").removeClass("active");
      $(".reset").addClass("active");
      resetAnimation(true);
    }
  });
  $(".right-box .buttons span").on("click", function () {
    if (startUserPattern) {
      userSelect($(this).attr("data-value"));
      const _this = $(this);
      _this.addClass("active");
      setTimeout(function () {
        _this.removeClass("active");
      }, 250);
    }
  });
});
