import "./assets/css/style.scss";
import React from "react";
import { Info } from "./components/info";
import { Game } from "./components/game";

const App = () => {
  return (
    <div className="container">
      <Info />
      <Game />
    </div>
  );
};

export default App;
