export const getRandom = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

export const globalCheckArrayEquals = (a, b) => {
  return a.length === b.length && a.every((val, index) => val === b[index]);
};
