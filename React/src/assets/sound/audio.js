import sound1 from "./1.mp3";
import sound2 from "./2.mp3";
import sound3 from "./3.mp3";
import sound4 from "./4.mp3";
import sound5 from "./5.mp3";
import sound6 from "./6.mp3";
import sound7 from "./7.mp3";
import sound8 from "./8.mp3";
import sound9 from "./9.mp3";
import sound10 from "./10.mp3";
import sound11 from "./11.mp3";
import sound12 from "./12.mp3";
import sound13 from "./13.mp3";
import sound14 from "./14.mp3";
import sound15 from "./15.mp3";
import sound16 from "./16.mp3";

const sounds = {
  sound1: new Audio(sound1),
  sound2: new Audio(sound2),
  sound3: new Audio(sound3),
  sound4: new Audio(sound4),
  sound5: new Audio(sound5),
  sound6: new Audio(sound6),
  sound7: new Audio(sound7),
  sound8: new Audio(sound8),
  sound9: new Audio(sound9),
  sound10: new Audio(sound10),
  sound11: new Audio(sound11),
  sound12: new Audio(sound12),
  sound13: new Audio(sound13),
  sound14: new Audio(sound14),
  sound15: new Audio(sound15),
  sound16: new Audio(sound16),
};

const play = (name) => {
  sounds[name].play();
};

const pause = (name) => {
  sounds[name].pause();
};

const exportObject = {
  pause,
  play,
  sounds,
};

export default exportObject;
