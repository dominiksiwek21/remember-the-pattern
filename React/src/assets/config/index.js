export const level = [
  {
    title: "Poziom łatwy",
    time: 800,
    shortcut: "Łatwy",
  },
  {
    title: "Poziom średni",
    time: 500,
    shortcut: "Średni",
  },
  {
    title: "Poziom trudny",
    time: 200,
    shortcut: "Trudny",
  },
  {
    title: "Poziom bardzo trudny",
    time: 75,
    shortcut: "Bardzo trudny",
  },
];
