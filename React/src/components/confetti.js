import React, { useEffect, useRef } from "react";
import ReactCanvasConfetti from "react-canvas-confetti/dist";

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
};

export const Confetti = ({ showConfetti }) => {
  const instance = useRef();

  const onInit = ({ confetti }) => {
    instance.current = confetti;
  };

  const makeShot = (particleRatio, opts) => {
    instance.current({
      ...opts,
      origin: { y: 0.5 },
      particleCount: Math.floor(200 * particleRatio),
    });
  };

  const fire = () => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  };

  useEffect(() => {
    if (showConfetti) {
      fire();
    }
  }, [showConfetti]);

  return <ReactCanvasConfetti style={canvasStyles} onInit={onInit} />;
};
