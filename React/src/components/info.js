import React from "react";
import { level } from "../assets/config";

export const Info = () => {
  return (
    <div className="info">
      <h1>Witaj!</h1>
      <p>
        Przedstawiam Ci krótką grę w której musisz naśladować kolejność
        wyświetlania przycisków z czarnego pola.
      </p>
      <p>
        Przed uruchomieniem gry, ustaw parametry (poziom trudnośći, ilość kroków
        i przycisków). Podczas gry nie można zmieniać tych ustawień.
      </p>
      <p>
        Sprawdzanie Twojego wzoru rozpocznie się w chwili kliknięcia takiej
        samej ilości przycisków jak w czarnym okienku. Jeżeli nie pamiętasz
        wzoru, kliknij dowolną kombinację przycisków lub zresetuj grę.
      </p>
      <br />
      <p>
        <strong>Start</strong> - Uruchamiasz grę z aktualnymi ustawieniami.
      </p>
      <p>
        <strong>Reset</strong> - Resetujesz grę wraz z ustawieniami. Przycisku
        reset możesz użyć dopiero po zakończeniu pokazywania wzoru.
      </p>
      <br />
      <p>Spis poziomów trudności:</p>
      <ul>
        {level.map((e) => (
          <li key={`Item${e.title}`}>
            <strong>{e.title}:</strong> Wyświetlanie elementów co {e.time}ms
          </li>
        ))}
      </ul>
    </div>
  );
};
