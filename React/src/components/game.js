import React, { useEffect, useRef, useState } from "react";
import { level } from "../assets/config";
import { getRandom, globalCheckArrayEquals } from "../assets/helpers/utils";
import audioButtons from "../assets/sound/audio";
import { Confetti } from "./confetti";

export const Game = () => {
  const [correct, setCorrect] = useState(5);
  const [gameLevel, setGameLevel] = useState(500);
  const [buttons, setButtons] = useState(16);
  const [animationActive, setAnimationActive] = useState(false);
  const [arrayAnimation, setArrayAnimation] = useState([]);
  const [userArrayAnimation, setUserArrayAnimation] = useState([]);
  const [blockButtons, setBlockButtons] = useState(false);
  const [endGame, setEndGame] = useState(false);
  const [startUserPattern, setStartUserPattern] = useState(false);
  const [activeButton, setActiveButton] = useState("0");
  const [playAnimation, setPlayAnimation] = useState(false);
  const [showConfetti, setShowConfetti] = useState(false);
  const animationButtonRef = useRef([]);
  const userButtonsRef = useRef([]);
  const leftPanelCorrectRef = useRef([]);
  const rightPanelCorrectRef = useRef([]);

  useEffect(() => {
    if (arrayAnimation.length > 0) showSteps();
  }, [arrayAnimation]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (
      userArrayAnimation.length > 0 &&
      arrayAnimation.length === userArrayAnimation.length
    ) {
      setStartUserPattern(false);
      setTimeout(() => {
        if (globalCheckArrayEquals(arrayAnimation, userArrayAnimation)) {
          timeoutUser();
        } else {
          setUserArrayAnimation([]);
          showSteps();
        }
      }, 500);
    }
  }, [userArrayAnimation]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (playAnimation) {
      startAnimation();
      setPlayAnimation(false);
    }
  }, [playAnimation]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (activeButton === "1") {
      startAnimation(false);
      clearCircle();
    } else if (activeButton === "2") {
      resetAnimation(false);
      clearCircle();
    }
  }, [activeButton]); // eslint-disable-line react-hooks/exhaustive-deps

  const resetAnimation = (f) => {
    if (!animationActive) {
      setArrayAnimation([]);
      setUserArrayAnimation([]);
      setBlockButtons(false);
      setAnimationActive(false);
      setStartUserPattern(false);
      setBlockButtons(false);
      setShowConfetti(false);
      if (f) {
        setCorrect(5);
        setGameLevel(500);
        setButtons(16);
        setActiveButton("2");
      }
    }
  };

  const startAnimation = (check) => {
    setShowConfetti(false);
    if (!animationActive && correct >= arrayAnimation.length + 1 && !check) {
      setAnimationActive(true);
      let random = getRandom(1, buttons + 1);
      if (random === arrayAnimation[arrayAnimation.length - 1]) {
        setAnimationActive(false);
        startAnimation(false);
      } else {
        setArrayAnimation([...arrayAnimation, random]);
      }
    }
    setBlockButtons(true);
  };

  const showSteps = () => {
    setBlockButtons(true);
    setStartUserPattern(false);
    arrayAnimation.map((e, i) => {
      setTimeout(
        () => {
          animationButtonRef.current[e - 1].className = "active";
          if (audioButtons.sounds[`sound${e}`]) audioButtons.play(`sound${e}`);
          setTimeout(() => {
            animationButtonRef.current[e - 1].className = "";
          }, gameLevel - 50);
        },
        i * gameLevel + 50,
      );
      return false;
    });
    setTimeout(
      () => {
        setAnimationActive(false);
        setStartUserPattern(true);
      },
      arrayAnimation.length * gameLevel + 200,
    );
  };

  const userSelect = (e) => {
    audioButtons.play(`sound${e}`);
    userButtonsRef.current[e - 1].className = "active";
    setUserArrayAnimation([...userArrayAnimation, e]);
    setTimeout(() => {
      userButtonsRef.current[e - 1].className = "";
    }, 250);
  };

  const timeoutUser = () => {
    if (Number(correct) === userArrayAnimation.length) {
      correctCircle();
      setEndGame(true);
      resetAnimation(false);
      setActiveButton("0");
      setShowConfetti(true);
      setTimeout(() => {
        if (
          window.confirm(
            "Gra zakończona. Gratulacje :) Wciśnij OK aby rozpocząć kolejną runde.",
          ) === true
        ) {
          setTimeout(() => {
            setActiveButton("1");
            startAnimation(false);
          }, 500);
        } else {
          resetAnimation(false);
        }
      }, 4000);
    } else {
      correctCircle();
      setUserArrayAnimation([]);
      setPlayAnimation(true);
    }
  };

  const correctCircle = () => {
    arrayAnimation.map((e, i) => {
      leftPanelCorrectRef.current[i].className = "green";
      rightPanelCorrectRef.current[i].className = "green";
      return false;
    });
  };

  const clearCircle = () => {
    [...leftPanelCorrectRef.current, ...rightPanelCorrectRef.current].map(
      (e) => (e ? (e.className = "") : false),
    );
  };

  return (
    <>
      <div className="panel">
        <div className="flex-select">
          <div>
            <label htmlFor="steps">Wybierz ilość kroków</label>
            <select
              id="steps"
              onChange={(e) => {
                !blockButtons
                  ? setCorrect(Number(e.target.value))
                  : alert(
                      "Uwaga! Masz uruchomioną grę. Zresetuj ustawienia a następnie wybierz ilość kroków.",
                    );
              }}
              value={correct}
            >
              {[...Array(10)].map((e, i) => (
                <option key={`Step${i + 1}`} value={Number(i + 1)}>
                  {i + 1}
                </option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="level">Wybierz poziom trudności</label>
            <select
              id="level"
              onChange={(e) => {
                setGameLevel(Number(e.target.value));
              }}
              value={gameLevel}
            >
              {level.map((e) => (
                <option value={e.time} key={`time${e.time}`}>
                  {e.shortcut}
                </option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="choose-buttons">Wybierz ilość przycisków</label>
            <select
              id="choose-buttons"
              onChange={(e) => {
                setButtons(Number(e.target.value));
              }}
              value={buttons}
            >
              {[...Array(buttons)].map((e, i) => (
                <option value={i + 1} key={`button${i + 1}`}>
                  {i + 1}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div>
          <button
            className={`start ${activeButton === "1" && "active"}`}
            onClick={() =>
              ((activeButton !== "1" && !animationActive) || endGame) &&
              setActiveButton("1")
            }
          >
            START
          </button>
          <button
            className={`reset ${activeButton === "2" && "active"}`}
            onClick={() => !animationActive && resetAnimation(true)}
          >
            RESET
          </button>
        </div>
      </div>
      <div className="boxes">
        <div className="box left-box">
          <div className="correct">
            {[...Array(correct)].map((e, i) => (
              <span
                key={`right${i}`}
                ref={(el) => (leftPanelCorrectRef.current[i] = el)}
              />
            ))}
          </div>
          <div className="buttons buttons-play">
            {[...Array(buttons)].map((e, i) => (
              <span
                key={`button-play${i + 1}`}
                data-value={i + 1}
                ref={(el) => (animationButtonRef.current[i] = el)}
              />
            ))}
          </div>
        </div>
        <div className="box right-box">
          <div className="correct">
            {[...Array(correct)].map((e, i) => (
              <span
                key={`left${i}`}
                ref={(el) => (rightPanelCorrectRef.current[i] = el)}
              />
            ))}
          </div>
          <div className="buttons">
            {[...Array(buttons)].map((e, i) => (
              <span
                key={`user-button${i + 1}`}
                data-value={i + 1}
                ref={(el) => (userButtonsRef.current[i] = el)}
                onClick={() => {
                  startUserPattern && userSelect(i + 1);
                }}
              />
            ))}
          </div>
        </div>
      </div>
      <Confetti showConfetti={showConfetti} />
    </>
  );
};
